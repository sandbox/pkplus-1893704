Module provides a form to search Google, Bing, Yahoo!, Alta Vista or Dogpile.

Install the module and activate it. You find it in the block section. 

No configuration is required, but you can change some settings. 

All configuration is done from block section.
